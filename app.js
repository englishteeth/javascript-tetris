
document.addEventListener('DOMContentLoaded', () => {
  const grid = document.querySelector('.grid');
  let squares = Array.from(document.querySelectorAll('.grid div'));
  const scoreDisplay = document.getElementById('score');
  const startBtn = document.getElementById('start-button');
  const width = 10;
  let nextRandom = 0;
  let timerId;
  let score =0;

  const colours = ['orange', 'red', 'purple', 'green', 'blue'];

  const lTetromino = [
    [1, width+1, width*2+1, 2],
    [width, width+1, width+2, width*2+2],
    [1, width+1, width*2+1, width*2],
    [width, width*2, width*2+1, width*2+2]
  ]

  const zTetromino = [
    [width+1, width+2, width*2, width*2+1],
    [0, width, width+1, width*2+1],
    [width+1, width+2, width*2, width*2+1],
    [0, width, width+1, width*2+1]
  ]

  const tTetromino = [
    [1, width, width+1, width+2],
    [1, width+1, width*2+1, width+2],
    [width, width+1, width+2, width*2+1],
    [width, 1, width+1, width*2+1],
  ]

  const oTetromino = [
    [0,1, width, width+1],
    [0,1, width, width+1],
    [0,1, width, width+1],
    [0,1, width, width+1]
  ]

  const iTetromino = [
    [1, width+1, width*2+1, width*3+1],
    [width,width+1,width+2,width+3],
    [1, width+1, width*2+1, width*3+1],
    [width,width+1,width+2,width+3]
  ]

  const theTetrominoes = [lTetromino, zTetromino, tTetromino, oTetromino, iTetromino];
  
  let currentPosition = 4;
  let currentRotation = 0

  let random = Math.floor(Math.random()* theTetrominoes.length);
  let current = theTetrominoes[random][currentRotation];

  function draw() {
    current.forEach(idx => {
      squares[currentPosition + idx].classList.add('tetromino');
      squares[currentPosition + idx].style.backgroundColor = colours[random];
    });
  }

  function undraw() {
    current.forEach(idx => {
      squares[currentPosition + idx].classList.remove('tetromino');
      squares[currentPosition + idx].style.backgroundColor = '';
    });

  }

  function control(e) {
    switch(e.keyCode) {
      case 37:
        moveLeft();
        break;
      case 38:
        rotate();
        break;
      case 39:
        moveRight();
        break;
      case 40:
        // moveDown();
        break;
      default:
        break;
    }
  }
  document.addEventListener('keyup', control);

  function moveDown() {
    undraw();
    currentPosition += width;
    draw();
    freeze();
  }

  function freeze() {
    if(current.some(idx => squares[currentPosition + idx + width].classList.contains('taken'))) {
      current.forEach(idx => squares[currentPosition + idx].classList.add('taken'))
      random = nextRandom;
      nextRandom = Math.floor(Math.random()* theTetrominoes.length);
      current = theTetrominoes[random][currentRotation];
      currentPosition = 4;
      draw();
      displayShape();
      addScore();
      gameOver();
    }
  } 

  function moveLeft() {
    undraw();
    const isAtLeftEdge = current.some(idx => (currentPosition + idx) % width === 0);
    if(!isAtLeftEdge) currentPosition -= 1;
    if(current.some(idx => squares[currentPosition + idx].classList.contains('taken'))) {
      currentPosition += 1;
    }
    draw();
  }

  function moveRight() {
    undraw();
    const isAtRightEdge = current.some(idx => (currentPosition + idx) % width === width - 1);
    if(!isAtRightEdge) currentPosition += 1;
    if(current.some(idx => squares[currentPosition + idx].classList.contains('taken'))) {
      currentPosition -= 1;
    }
    draw();
  }

  function rotate() {
    undraw();
    currentRotation++;
    if(currentRotation === current.length) currentRotation = 0;
    current = theTetrominoes[random][currentRotation];
    draw();
  }

  const displaySquares = document.querySelectorAll('.mini-grid div');
  const displayWidth = 4;
  const displayIndex = 0;

  const upNextTerominoes = [
    [1, displayWidth+1, displayWidth*2+1, 2], // lTetromino
    [0, displayWidth, displayWidth+1, displayWidth*2+1], //zTetromino
    [1, displayWidth, displayWidth+1, displayWidth+2 ], //tTetromino
    [0, 1, displayWidth, displayWidth+1], //oTetromino
    [1, displayWidth+1, displayWidth*2+1, displayWidth*3+1] //iTetromino
  ]

  function displayShape() {
    displaySquares.forEach(square => {
      square.classList.remove('tetromino');
      square.style.backgroundColor = '';
    });
    upNextTerominoes[nextRandom].forEach(idx => {
      displaySquares[displayIndex + idx].classList.add('tetromino');
      displaySquares[displayIndex + idx].style.backgroundColor = colours[nextRandom];
    })
  }

  startBtn.addEventListener('click', () => {
    if (timerId) {
      clearInterval(timerId);
      timerId = null;
    } else {
      draw();
      timerId = setInterval(moveDown, 500);
      nextRandom = Math.floor(Math.random()* theTetrominoes.length);
      displayShape();
    }
  })

  function addScore() {
    for(let i=0; i < 199; i += width ) {
      const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9]
      if(row.every( idx => squares[idx].classList.contains('taken'))){
        score += 10;
        scoreDisplay.innerHTML = score;
        row.forEach(idx => {
          squares[idx].classList.remove('taken');
          squares[idx].classList.remove('tetromino');
          squares[idx].style.backgroundColor = '';
        })
        const squaresRemoved = squares.splice(i, width);
        // console.log(squaresRemoved);
        squares = squaresRemoved.concat(squares)
        squares.forEach(cell => grid.appendChild(cell));
      }
    }
  }

  function gameOver() {
    if(current.some(idx => squares[currentPosition + idx].classList.contains('taken'))) {
      scoreDisplay.innerHTML = 'end';
      clearInterval(timerId);
    }
  }

});
